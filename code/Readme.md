# Project Title
---
Authors: Miguel, Roger and Adrian Hernandez

This repo corresponds to the final project for CAP-5768: Introduction to Data Science. 

# Directory Structure
---

* Data
* Scripts
    * Contains the helper scripts used for data cleaning, compilation of new datasets from different datasources etc. 
* Visualizations
    * Contains notebooks with different vizualizations of the datasets.
* Models
    * Contains notebooks use for training of different ML models. 